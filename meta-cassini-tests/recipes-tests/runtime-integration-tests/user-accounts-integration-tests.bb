# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "User accounts integration tests."
DESCRIPTION = "Integration tests for user accounts settings validation. \
               Tests may be run standalone via \
               run-user-accounts-integration-tests, or via the ptest \
               framework using ptest-runner."
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"

LICENSE = "MIT"
# License file is in "layers/poky/meta/files/common-licenses".
# nooelint: oelint.var.licenseremotefile
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

OVERRIDES:append = " ${CASSINI_OVERRIDES}"

TEST_FILES = "file://user-accounts-integration-tests.bats \
              file://user-accounts-funcs.sh \
              "

# nooelint: oelint.vars.specific - Distro feature name
TEST_FILES:append:cassini-security = " \
    file://user-accounts-append-security.bats \
    file://user-accounts-security-funcs.sh \
    "

inherit runtime-integration-tests
require runtime-integration-tests.inc

export CASSINI_SECURITY_UMASK
# nooelint: oelint.vars.specific - Distro feature name
ENVSUBST_VARS:append:cassini-security = " \$CASSINI_SECURITY_UMASK"

do_install[vardeps] += "\
    CASSINI_SECURITY_UMASK \
    "
