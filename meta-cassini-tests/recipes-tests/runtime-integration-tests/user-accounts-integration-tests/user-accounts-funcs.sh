#!/usr/bin/env bash
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

check_user_home_dir_mode() {
    dir_mode="$(stat -c "%A" "/home/${1}" 2>"${TEST_STDERR_FILE}")"
    if [ "${dir_mode:4}" = "------" ]; then
        return 0
    else
        echo "Directory '/home/${1}' current mode: '${dir_mode}'," \
             "expected mode: 'drwx------'."
        return 1
    fi
}

check_user_sudo_privileges() {
    sudo su "${1}" -c 'sudo -nv' 2>"${TEST_STDERR_FILE}"
}
