# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend := "${@bb.utils.contains('DISTRO_FEATURES', \
                                                 'cassini-test', \
                                                 '${THISDIR}/files:', \
                                                 '', \
                                                 d)}"

# As Bats requires the nl utility, configure CONFIG_NL=y for busybox if we are
# using Bats
# nooelint: oelint.vars.inconspaces,oelint.vars.listappend - Space only added if patch required
SRC_URI:append := "${@bb.utils.contains('DISTRO_FEATURES', \
                                        'cassini-test', \
                                        ' file://nl.cfg', \
                                        '', \
                                        d)}"
