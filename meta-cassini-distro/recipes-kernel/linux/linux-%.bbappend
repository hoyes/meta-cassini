# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

FILESEXTRAPATHS:prepend:libc-glibc:cassini := "${THISDIR}:${THISDIR}/linux-yocto:"

#
# cassini kmeta
#

SRC_URI:append:libc-glibc:cassini = " file://cassini-kmeta;type=kmeta;name=cassini-kmeta;destsuffix=cassini-kmeta"

# Add gator daemon kernel configs dependencies.
# nooelint: oelint.vars.inconspaces - Space only added if distro feature is active
KERNEL_FEATURES:append:libc-glibc:aarch64:cassini = "${@bb.utils.contains('DISTRO_FEATURES', \
        'cassini-sdk', \
        ' features/cassini/gator.scc', '', d)}"
