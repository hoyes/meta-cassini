# nooelint: oelint.var.mandatoryvar - This recipe has no source files
#
# SPDX-FileCopyrightText: <text>Copyright 2022-2023 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: MIT

SUMMARY = "Firmware image deploying multi-config firmware"
DESCRIPTION = "Image for deploying a firmware set on platforms using multi-config"
HOMEPAGE = "https://cassini.readthedocs.io/en/latest/"
LICENSE = "MIT"

inherit deploy nopackages

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(corstone1000-mps3|corstone1000-fvp)"
do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_install[noexec] = "1"

FIRMWARE_BINARIES = "corstone1000-image-${MACHINE}.wic \
                     bl1.bin \
                     es_flashfw.bin \
                     ${@ 'corstone1000-utils-overlay-image-${MACHINE}.tar.bz2' \
                     if d.getVar('MACHINE') == 'corstone1000-mps3' else ''} \
                    "

do_deploy() {
    firmware_loc="${TOPDIR}/tmp-firmware/deploy/images/${MACHINE}"
    for firmware in ${FIRMWARE_BINARIES}; do
        cp -av ${firmware_loc}/${firmware} ${DEPLOYDIR}/
        if [ -L ${firmware_loc}/${firmware} ]; then
            cp -av ${firmware_loc}/$(readlink ${firmware_loc}/${firmware}) ${DEPLOYDIR}/
        fi
    done
}

do_deploy[umask] = "022"
do_deploy[mcdepends] = "mc::firmware:corstone1000-image:do_image_complete \
                        ${@ 'mc::firmware:corstone1000-utils-overlay-image:do_image_complete' \
                        if d.getVar('MACHINE') == 'corstone1000-mps3' else ''} \
                       "

addtask deploy after do_prepare_recipe_sysroot
